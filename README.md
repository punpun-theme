## About

This repository is no longer maintained. Please uninstall the
`punpun-theme` package and install the [punpun-themes] package (note:
pluralization) instead. See [melpa/melpa #8219] for the reasoning
behind this change.

[melpa/melpa #8219]: https://github.com/melpa/melpa/issues/8219
[punpun-themes]: https://depp.brause.cc/punpun-themes/
